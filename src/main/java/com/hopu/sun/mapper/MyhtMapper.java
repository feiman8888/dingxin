package com.hopu.sun.mapper;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.hopu.sun.model.Myht;

import java.util.List;


public interface MyhtMapper extends BaseMapper<Myht> {
    //查询所有
    List<Myht> findAllht();

    //添加
    int addHt(Myht entity);

    //根据ID查询
    Myht findByHid(Myht entity);

    //修改
    int uppHt(Myht entity);
}
