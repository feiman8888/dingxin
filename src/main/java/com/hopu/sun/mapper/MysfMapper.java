package com.hopu.sun.mapper;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.hopu.sun.model.Mysf;

import java.util.List;

public interface MysfMapper extends BaseMapper<Mysf> {
    //查询所有
    List<Mysf> findAllDjsf(Mysf entity);

    //根据ID查询
    List<Mysf> findAllSfByMid(Mysf entity);

    //添加
    int addSfXf(Mysf entity);
}
