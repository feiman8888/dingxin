package com.hopu.sun.mapper;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.hopu.sun.model.Mycus;

import java.util.List;

public interface MycusMapper extends BaseMapper<Mycus> {

    //查询所有
    List<Mycus> findAllCustom();

    //添加
    int addCus(Mycus entity);

    //修改
    int uppCus(Mycus entity);

    //根据ID查询
    Mycus findCusById(Mycus entity);

}
