package com.hopu.sun.mapper;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.hopu.sun.model.Myzhichu;

import java.util.List;


public interface MyzhichuMapper extends BaseMapper<Myzhichu> {
    //查询所有
    List<Myzhichu> findAllzhichu();

    //添加
    int addZhichu(Myzhichu entity);

    //根据ID查询
    Myzhichu findZhichuByZid(Myzhichu entity);

    //修改
    int updateZhichu(Myzhichu entity);

}
