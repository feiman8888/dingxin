package com.hopu.sun.mapper;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.hopu.sun.model.Mydj;

import java.util.List;


public interface MydjMapper extends BaseMapper<Mydj> {

    //查询所有
    List<Mydj> findAllDjrz(Mydj entity);

    //添加
    int addDj(Mydj entity);

    //修改
    int updateDj(Mydj entity);

    //根据ID查询
    Mydj findDjByMid(Mydj entity);

    //修改状态
    int updateTz(Mydj entity);

    //条件查询
    List<Mydj> findAllDjrzByCondition(Mydj entity);

    //到期查询
    List<Mydj> findAllMydjOnDaoqi(Mydj entity);


}
