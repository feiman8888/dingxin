package com.hopu.sun.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.hopu.sun.model.Myarea;

import java.util.List;


public interface MyareaMapper extends BaseMapper<Myarea> {

    //查询所有区域
    List<Myarea> findAllAreaPageData();

    //添加
    int addArea(Myarea entity);

    //修改
    int uppArea(Myarea entity);

    //根据条件查询
    Myarea getArea(Myarea entity);
}