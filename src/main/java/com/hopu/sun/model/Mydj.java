package com.hopu.sun.model;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class Mydj implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "mid", type = IdType.AUTO)
    private Long mid;

    private String mdate;

    private Integer eid;

    private Long cid;

    private Long hid;

    private String mimg;

    private Float myj;

    private Float myzj;

    private Integer mflag;

    private String mbegintime;

    private Myhouse myhouse;

    private Myemp myemp;

    private Mycus mycus;

    private Mysf mysf;
}
