package com.hopu.sun.model;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class Myzhichu implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "zid", type = IdType.AUTO)
    private Long zid;

    private Integer eid;

    private Float zmoney;

    private String ztm;

    private String ztime;

    private String zremark;

    private Myemp myemp;


}
