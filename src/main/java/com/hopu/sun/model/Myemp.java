package com.hopu.sun.model;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class Myemp implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "eid", type = IdType.AUTO)
    private Integer eid;

    private Integer pid;

    private Integer jid;

    private String ename;

    private String epsw;

    private String erealname;

    private String etel;

    private String eaddress;

    private Integer eflag;

    private String eremark;

    private Myjs myjs;

    private Mydept mydept;


}
