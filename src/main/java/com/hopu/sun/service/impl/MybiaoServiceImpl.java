package com.hopu.sun.service.impl;


import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.hopu.sun.mapper.MybiaoMapper;
import com.hopu.sun.model.Mybiao;
import com.hopu.sun.service.MybiaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MybiaoServiceImpl extends ServiceImpl<MybiaoMapper, Mybiao> implements MybiaoService {
    @Autowired
    private MybiaoMapper mybiaoMapper;

    //查询
    @Override
    public List<Mybiao> findAllBiao() {
        return mybiaoMapper.findAllBiao();
    }

    //添加
    @Override
    public int addBiao(Mybiao entity) {
        return mybiaoMapper.addBiao(entity);
    }

    //修改
    @Override
    public int updateMybiao(Mybiao entity) {
        return mybiaoMapper.updateMybiao(entity);
    }

    //根据ID查
    @Override
    public Mybiao findBiaoByBid(Mybiao entity) {
        return mybiaoMapper.findBiaoByBid(entity);
    }


}
