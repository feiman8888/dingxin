package com.hopu.sun.service.impl;


import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.hopu.sun.mapper.MytfMapper;
import com.hopu.sun.model.Mytf;
import com.hopu.sun.service.MytfService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class MytfServiceImpl extends ServiceImpl<MytfMapper, Mytf> implements MytfService {
    @Autowired
    private MytfMapper mytfMapper;

    //退租
    @Override
    public int tf(Mytf entity) {
        return mytfMapper.tf(entity);
    }

}
