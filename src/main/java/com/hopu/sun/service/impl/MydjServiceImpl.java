package com.hopu.sun.service.impl;


import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.hopu.sun.mapper.MydjMapper;
import com.hopu.sun.model.Mydj;
import com.hopu.sun.service.MydjService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class MydjServiceImpl extends ServiceImpl<MydjMapper, Mydj> implements MydjService {
    @Autowired
    private MydjMapper mydjMapper;

    //查询
    @Override
    public List<Mydj> findAllDjrz(Mydj entity) {
        return mydjMapper.findAllDjrz(entity);
    }

    //根据ID查询
    @Override
    public Mydj findDjByMid(Mydj entity) {
        return mydjMapper.findDjByMid(entity);
    }

    //添加
    @Override
    public int addDj(Mydj entity) {
        return mydjMapper.addDj(entity);
    }

    //修改
    @Override
    public int updateDj(Mydj entity) {
        return mydjMapper.updateDj(entity);
    }

    //修改状态
    @Override
    public int updateTz(Mydj entity) {
        return mydjMapper.updateDj(entity);
    }

    //条件查询
    @Override
    public List<Mydj> findAllDjrzByCondition(Mydj entity) {
        return mydjMapper.findAllDjrzByCondition(entity);
    }

    //到期查询
    @Override
    public List<Mydj> findAllMydjOnDaoqi(Mydj entity) {
        return mydjMapper.findAllMydjOnDaoqi(entity);
    }

}
