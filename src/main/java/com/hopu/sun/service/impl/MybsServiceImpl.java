package com.hopu.sun.service.impl;


import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.hopu.sun.mapper.MybsMapper;
import com.hopu.sun.model.Mybs;
import com.hopu.sun.service.MybsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class MybsServiceImpl extends ServiceImpl<MybsMapper, Mybs> implements MybsService {
    @Autowired
    private MybsMapper mybsMapper;

    //查询
    @Override
    public List<Mybs> findAllBs() {
        return mybsMapper.findAllBs();
    }

    //添加
    @Override
    public int addBs(Mybs entity) {
        return mybsMapper.addBs(entity);
    }

    //修改
    @Override
    public int updateBs(Mybs entity) {
        return mybsMapper.updateBs(entity);
    }

    //根据ID查询
    @Override
    public Mybs findBsByBid(Mybs entity) {
        return mybsMapper.findBsByBid(entity);
    }
}
