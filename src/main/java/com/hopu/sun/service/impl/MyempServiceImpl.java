package com.hopu.sun.service.impl;


import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.hopu.sun.mapper.MyempMapper;
import com.hopu.sun.model.Myemp;
import com.hopu.sun.service.MyempService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author SunNan
 * @since 2020-09-05
 */
@Service
public class MyempServiceImpl extends ServiceImpl<MyempMapper, Myemp> implements MyempService {

    @Autowired
    MyempMapper myempMapper;

    //查询
    @Override
    public List<Myemp> findAllEmp(Myemp entity) {
        return myempMapper.findAllEmp(entity);
    }

    //登录
    @Override
    public Myemp login(Myemp entity) {
        return myempMapper.login(entity);
    }

    //根据名称查询
    @Override
    public Myemp findByEname(Myemp entity) {
        return myempMapper.findByEname(entity);
    }

    //添加
    @Override
    public int addEmp(Myemp entity) {
        return myempMapper.addEmp(entity);
    }

    //修改
    @Override
    public int uppEmp(Myemp entity) {
        return myempMapper.uppEmp(entity);
    }

    //根据ID查询
    @Override
    public Myemp findEmpById(Myemp entity) {
        return myempMapper.findEmpById(entity);
    }

    //条件查询
    @Override
    public Myemp findEmpByss(Myemp entity) {
        return myempMapper.findEmpByss(entity);
    }

    //修改密码
    @Override
    public int uppPwd(Myemp entity) {
        return myempMapper.uppPwd(entity);
    }

}
