package com.hopu.sun.service.impl;


import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.hopu.sun.common.Msg;
import com.hopu.sun.mapper.MyjsMapper;
import com.hopu.sun.model.Myjs;
import com.hopu.sun.service.MyjsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MyjsServiceImpl extends ServiceImpl<MyjsMapper, Myjs> implements MyjsService {

    @Autowired
    MyjsMapper myjsMapper;

    //查询
    public List<Myjs> findAllJs() {
        return myjsMapper.findAllJs();
    }

    //添加
    public Msg addJs(Myjs entity) {
        int i = myjsMapper.addJs(entity);
        if (i > 0) {
            return new Msg(200, "添加成功");
        } else {
            return new Msg(500, "添加失败");
        }
    }

    //修改
    public Msg updateJs(Myjs entity) {
        int i = myjsMapper.updateJs(entity);
        if (i > 0) {
            return new Msg(200, "修改成功");
        } else {
            return new Msg(500, "修改失败");
        }
    }

    //根据ID查询
    public Msg findJs(Myjs entity) {
        Myjs js = myjsMapper.findJs(entity);
        if (js != null) {
            return new Msg(200, "成功").add("list", js);
        } else {
            return new Msg(500, "失败");
        }
    }

}
