package com.hopu.sun.service;

import com.baomidou.mybatisplus.service.IService;
import com.hopu.sun.model.Myhouse;

import java.util.List;


public interface MyhouseService extends IService<Myhouse> {
    //查询所有
    List<Myhouse> findAllHousePD();

    //根据ID查询
    Myhouse findHouseById(Myhouse entity);

    //添加
    int addHouse(Myhouse entity);

    //修改
    int uppHouse(Myhouse entity);

    //修改状态
    int updatehouse(Myhouse entity);

    //条件查询
    List<Myhouse> findAllHouseByCondition(Myhouse entity);

    //ID查询
    List<Myhouse> findAllHouseByAidAndSid(Myhouse entity);
}
