package com.hopu.sun.service;

import com.baomidou.mybatisplus.service.IService;
import com.hopu.sun.model.Mydept;

import java.util.List;


public interface MydeptService extends IService<Mydept> {

    List<Mydept> findAllDept(Mydept entity);

    //添加
    int addPart(Mydept entity);

    //修改
    int updatePart(Mydept entity);

    //根据条件查询
    Mydept findPart(Mydept entity);
}

