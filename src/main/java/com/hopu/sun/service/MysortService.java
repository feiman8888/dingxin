package com.hopu.sun.service;

import com.baomidou.mybatisplus.service.IService;
import com.hopu.sun.model.Mysort;

import java.util.List;

public interface MysortService extends IService<Mysort> {
    //查询所有
    List<Mysort> findMysort();

    //添加
    int addSort(Mysort entity);

    //修改
    int uppSort(Mysort entity);

    //根据条件查询
    Mysort findSortByNameOrId(Mysort entity);

}
