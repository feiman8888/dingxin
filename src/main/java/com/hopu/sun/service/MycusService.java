package com.hopu.sun.service;

import com.baomidou.mybatisplus.service.IService;
import com.hopu.sun.model.Mycus;

import java.util.List;

public interface MycusService extends IService<Mycus> {
    //查询
    List<Mycus> findAllCustom();

    //添加
    int addPart(Mycus entity);

    //修改
    int uppCus(Mycus entity);

    //根据ID查询
    Mycus findCusById(Mycus entity);

}
