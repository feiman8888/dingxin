package com.hopu.sun.controller;

import com.baomidou.mybatisplus.plugins.pagination.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hopu.sun.common.Msg;
import com.hopu.sun.model.Myemp;
import com.hopu.sun.service.MyempService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@Controller
@RequestMapping("/sun/myemp")
public class MyempController {

    @Autowired
    MyempService myempService;

    //登录
    @RequestMapping("/login.do")
    @ResponseBody
    public Msg selectLogin(HttpServletRequest request, Myemp entity) {
        Myemp login = myempService.login(entity);
        if (login != null) {
            request.getSession().setAttribute("login", login);
            return new Msg(200, "登录成功！");
        } else {
            return new Msg(500, "登录失败，请检查账号密码后重试！");
        }
    }


    //根据账号查询所有人员
    @RequestMapping("/findByEname.do")
    @ResponseBody
    public Msg findByEname(Myemp entity) {
        Myemp byEname = myempService.findByEname(entity);
        if (byEname != null) {
            return new Msg(200, "");
        } else {
            return new Msg(500, "");
        }
    }

    @RequestMapping("/findEmpByss.do")
    @ResponseBody
    public Msg findEmpByss(Myemp entity, HttpServletRequest request) {
        Myemp myemp = (Myemp) request.getSession().getAttribute("login");
        entity.setEid(myemp.getEid());
        Myemp list = myempService.findEmpByss(entity);
        if (list != null) {
            return new Msg(200, "成功！").add("list", list);
        } else {
            return new Msg(500, "失败！");
        }
    }

    //查询
    @RequestMapping("/emp/findAllEmp.do")
    @ResponseBody
    private Msg findAllEmp(Myemp myemp,
                           @RequestParam(name = "current", defaultValue = "1", required = true) Integer current,
                           @RequestParam(name = "pageSize", defaultValue = "14") Integer pageSize
    ) {
        PageHelper.startPage(current, pageSize);
        List<Myemp> list = myempService.findAllEmp(myemp);
        PageInfo<Myemp> pageInfo = new PageInfo<>(list);
        if (pageInfo != null) {
            return new Msg(200, "").add("pageInfo", pageInfo);
        } else {
            return new Msg(500, "查询失败！");
        }


    }

    //删除
    @RequestMapping("/emp/delEmp.do")
    @ResponseBody
    public Msg delEmp(Integer eid) {
        Myemp myemp = new Myemp();
        myemp.setEflag(1);
        myemp.setEid(eid);
        boolean b = myempService.updateById(myemp);
        if (b == true) {
            return new Msg(200, "删除成功！");
        } else {
            return new Msg(500, "删除失败！");
        }
    }

    //恢复
    @RequestMapping("/emp/huiFuEmp.do")
    @ResponseBody
    public Msg huiFuEmp(Integer eid) {
        Myemp myemp = new Myemp();
        myemp.setEflag(0);
        myemp.setEid(eid);
        boolean b = myempService.updateById(myemp);
        if (b == true) {
            return new Msg(200, "恢复成功！");
        } else {
            return new Msg(500, "恢复失败！");
        }
    }

    //添加
    @RequestMapping("/emp/addEmp.do")
    @ResponseBody
    public Msg addEmp(Myemp eneity) {
        eneity.setEflag(0);
        int i = myempService.addEmp(eneity);
        if (i > 0) {
            return new Msg(200, "添加成功！");
        } else {
            return new Msg(500, "添加失败！");
        }
    }

    //修改
    @RequestMapping("/emp/uppEmp.do")
    @ResponseBody
    public Msg uppEmp(Myemp eneity) {
        int i = myempService.uppEmp(eneity);
        if (i > 0) {
            return new Msg(200, "修改成功！");
        } else {
            return new Msg(500, "修改失败！");
        }
    }

    //根据ID查询
    @RequestMapping("/emp/findEmpById.do")
    @ResponseBody
    public Msg findEmpById(Myemp eneity) {
        Myemp empById = myempService.findEmpById(eneity);
        if (empById != null) {
            return new Msg(200, "成功").add("empById", empById);
        } else {
            return new Msg(500, "失败");
        }
    }

    //修改密码
    @RequestMapping("/uppPwd.do")
    @ResponseBody
    public Msg uppPwd(Myemp eneity, HttpServletRequest request) {
        Myemp myemp = (Myemp) request.getSession().getAttribute("login");
        eneity.setEid(myemp.getEid());
        int i = myempService.uppPwd(eneity);
        if (i > 0) {
            return new Msg(200, "修改成功！");
        } else {
            return new Msg(500, "修改失败！");
        }
    }

    //退出
    @RequestMapping("/logout.do")
    public void logout(HttpServletRequest request, HttpServletResponse response) throws IOException {
        request.getSession().setAttribute("login", null);
        response.sendRedirect(request.getContextPath()+"/index.jsp");
    }

    @RequestMapping("/redirect.do")
    public String redirect(String page) {
        return page;
    }
}

