package com.hopu.sun.controller;


import com.baomidou.mybatisplus.plugins.pagination.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hopu.sun.common.Msg;
import com.hopu.sun.model.Mybiao;
import com.hopu.sun.model.Myemp;
import com.hopu.sun.service.MybiaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
public class MybiaoController {
    @Autowired
    private MybiaoService mybiaoService;

    //查询
    @RequestMapping("/biao/findAllBiao.do")
    @ResponseBody
    private Msg findAllBiao(
            @RequestParam(name = "current", defaultValue = "1", required = true) Integer current,
            @RequestParam(name = "pageSize", defaultValue = "14") Integer pageSize
    ) {
        PageHelper.startPage(current, pageSize);
        List<Mybiao> list = mybiaoService.findAllBiao();
        PageInfo<Mybiao> pageInfo = new PageInfo<>(list);
        if (pageInfo != null) {
            return new Msg(200, "").add("pageInfo", pageInfo);
        } else {
            return new Msg(500, "查询失败！");
        }
    }

    //添加
    @RequestMapping("/biao/addBiao.do")
    @ResponseBody
    public Msg addBiao(Mybiao entity, HttpServletRequest request) {
        //获取登录人id
        Myemp myemp = (Myemp) request.getSession().getAttribute("login");
        entity.setEid(myemp.getEid());
        //获取当前的日期
        Date date = new Date();
        //设置要获取到的时间年月日
        SimpleDateFormat time = new SimpleDateFormat("yyyy-MM-dd");
        //获取String类型的时间
        entity.setMtime(time.format(date));
        int i = mybiaoService.addBiao(entity);
        if (i > 0) {
            return new Msg(200, "成功！");
        } else {
            return new Msg(500, "失败！");
        }
    }

    //根据ID查询
    @RequestMapping("/biao/findBiaoByBid.do")
    @ResponseBody
    public Msg findBiaoByBid(Mybiao entity) {
        Mybiao byBid = mybiaoService.findBiaoByBid(entity);
        if (byBid != null) {
            return new Msg(200, "成功！").add("byBid", byBid);
        } else {
            return new Msg(500, "失败！");
        }
    }

    //修改
    @RequestMapping("/biao/updateMybiao.do")
    @ResponseBody
    public Msg updateMybiao(Mybiao entity, HttpServletRequest request) {
        //获取登录人id
        Myemp myemp = (Myemp) request.getSession().getAttribute("login");
        entity.setEid(myemp.getEid());
        //获取当前的日期
        Date date = new Date();
        //设置要获取到的时间年月日
        SimpleDateFormat time = new SimpleDateFormat("yyyy-MM-dd");
        //获取String类型的时间
        entity.setMtime(time.format(date));
        int i = mybiaoService.updateMybiao(entity);
        if (i > 0) {
            return new Msg(200, "成功！");
        } else {
            return new Msg(500, "失败！");
        }
    }

}

