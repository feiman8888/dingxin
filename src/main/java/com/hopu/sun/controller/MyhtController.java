package com.hopu.sun.controller;


import com.baomidou.mybatisplus.plugins.pagination.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hopu.sun.common.Msg;
import com.hopu.sun.model.Myht;
import com.hopu.sun.service.MyhtService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class MyhtController {
    @Autowired
    private MyhtService myhtService;

    //查询
    @RequestMapping("/ht/findAllht.do")
    @ResponseBody
    private Msg findAllht(
            @RequestParam(name = "current", defaultValue = "1", required = true) Integer current,
            @RequestParam(name = "pageSize", defaultValue = "14") Integer pageSize
    ) {
        PageHelper.startPage(current, pageSize);
        List<Myht> list = myhtService.findAllht();
        PageInfo<Myht> pageInfo = new PageInfo<>(list);
        if (pageInfo != null) {
            return new Msg(200, "").add("pageInfo", pageInfo);
        } else {
            return new Msg(500, "查询失败！");
        }
    }

    //添加
    @RequestMapping("/ht/addHt.do")
    @ResponseBody
    public Msg addHt(Myht entity) {
        int i = myhtService.addHt(entity);
        if (i > 0) {
            return new Msg(200, "成功");
        } else {
            return new Msg(500, "失败");
        }
    }

    //根据ID查询
    @RequestMapping("/ht/findByHid.do")
    @ResponseBody
    public Msg findByHid(Myht entity) {
        Myht list = myhtService.findByHid(entity);
        if (list != null) {
            return new Msg(200, "成功").add("list", list);
        } else {
            return new Msg(500, "失败");
        }
    }

    //修改
    @RequestMapping("/ht/uppHt.do")
    @ResponseBody
    public Msg uppHt(Myht entity) {
        int i = myhtService.uppHt(entity);
        if (i > 0) {
            return new Msg(200, "成功");
        } else {
            return new Msg(500, "失败");
        }
    }
}

