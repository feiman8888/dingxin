package com.hopu.sun.controller;


import com.baomidou.mybatisplus.plugins.pagination.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hopu.sun.common.Msg;
import com.hopu.sun.common.OssUtil;
import com.hopu.sun.model.Mydj;
import com.hopu.sun.model.Myemp;
import com.hopu.sun.model.Mysf;
import com.hopu.sun.service.MydjService;
import com.hopu.sun.service.MysfService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;


@Controller
public class MysfController {
    @Autowired
    private MysfService mysfService;
    @Autowired
    private MydjService mydjService;

    //查询
    @RequestMapping("/djsf/findAllDjsf.do")
    @ResponseBody
    private Msg findAllDjsf(Mysf entity,
                            @RequestParam(name = "current", defaultValue = "1", required = true) Integer current,
                            @RequestParam(name = "pageSize", defaultValue = "14") Integer pageSize
    ) {
        PageHelper.startPage(current, pageSize);
        List<Mysf> list = mysfService.findAllDjsf(entity);
        for (Mysf im : list) {
            OssUtil ossUtil = new OssUtil();
            String imgs = im.getMydj().getMimg();
            imgs = ossUtil.showPic(imgs);

            im.getMydj().setMimg(imgs);
        }
        PageInfo<Mysf> pageInfo = new PageInfo<>(list);
        if (pageInfo != null) {
            return new Msg(200, "").add("pageInfo", pageInfo);
        } else {
            return new Msg(500, "查询失败！");
        }
    }

    //根据ID查询
    @RequestMapping("/djsf/findAllSfByMid.do")
    @ResponseBody
    private Msg findAllSfByMid(Mysf entity,
                               @RequestParam(name = "current", defaultValue = "1", required = true) Integer current,
                               @RequestParam(name = "pageSize", defaultValue = "14") Integer pageSize
    ) {
        PageHelper.startPage(current, pageSize);
        List<Mysf> list = mysfService.findAllSfByMid(entity);
        PageInfo<Mysf> pageInfo = new PageInfo<>(list);
        if (pageInfo != null) {
            return new Msg(200, "").add("pageInfo", pageInfo);
        } else {
            return new Msg(500, "查询失败！");
        }
    }

    //续费
    @RequestMapping("/djsf/addSfXf.do")
    @ResponseBody
    public Msg addSfXf(Mysf mysf, HttpServletRequest request) {
        Mydj mydj = new Mydj();
        mydj.setMflag(0);
        //获取登录人id
        Myemp myemp = (Myemp) request.getSession().getAttribute("login");
        mysf.setEid(myemp.getEid());
        int i1 = mydjService.updateDj(mydj);
        int i = mysfService.addSfXf(mysf);
        if (i > 0) {
            return new Msg(200, "成功！");
        } else {
            return new Msg(500, "失败！");
        }

    }

}

