package com.hopu.sun.controller;


import com.baomidou.mybatisplus.plugins.pagination.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hopu.sun.common.Msg;
import com.hopu.sun.model.Myarea;
import com.hopu.sun.service.MyareaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/sun/myarea")
public class MyareaController {

    @Autowired
    private MyareaService myareaService;

    @RequestMapping("/redirect")
    public String redirect(String page) {
        return page;
    }

    //查询
    @RequestMapping("/area/findAllAreaPageData.do")
    @ResponseBody
    private Msg findAllAreaPageData(
            @RequestParam(name = "current", defaultValue = "1", required = true) Integer current,
            @RequestParam(name = "pageSize", defaultValue = "14") Integer pageSize
    ) {
        PageHelper.startPage(current, pageSize);
        List<Myarea> list = myareaService.findAllAreaPageData();
        PageInfo<Myarea> pageInfo = new PageInfo<>(list);
        if (pageInfo != null) {
            return new Msg(200, "").add("pageInfo", pageInfo);
        } else {
            return new Msg(500, "查询失败！");
        }
    }

    //根据条件查询
    @RequestMapping("/area/getArea.do")
    @ResponseBody
    public Msg getArea(Myarea entity) {
        Myarea area = myareaService.getArea(entity);
        if (area != null) {
            return new Msg(200, "成功！").add("area", area);
        } else {
            return new Msg(500, "失败！");
        }
    }

    //添加
    @RequestMapping("/area/addArea.do")
    @ResponseBody
    public Msg addArea(Myarea entity) {
        int i = myareaService.addArea(entity);
        if (i > 0) {
            return new Msg(200, "添加成功！");
        } else {
            return new Msg(500, "添加失败！");
        }
    }

    //修改
    @RequestMapping("/area/uppArea.do")
    @ResponseBody
    public Msg uppArea(Myarea entity) {
        int i = myareaService.uppArea(entity);
        if (i > 0) {
            return new Msg(200, "修改成功！");
        } else {
            return new Msg(500, "修改失败！");
        }
    }

}

