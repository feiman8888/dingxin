package com.hopu.sun.controller;


import com.baomidou.mybatisplus.plugins.pagination.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hopu.sun.common.Msg;
import com.hopu.sun.model.Myemp;
import com.hopu.sun.model.Myzhichu;
import com.hopu.sun.service.MyzhichuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
public class MyzhichuController {
    @Autowired
    private MyzhichuService myzhichuService;

    //查询
    @RequestMapping("/zhichu/findAllzhichu.do")
    @ResponseBody
    private Msg findAllzhichu(
            @RequestParam(name = "current", defaultValue = "1", required = true) Integer current,
            @RequestParam(name = "pageSize", defaultValue = "14") Integer pageSize
    ) {
        PageHelper.startPage(current, pageSize);
        List<Myzhichu> list = myzhichuService.findAllzhichu();
        PageInfo<Myzhichu> pageInfo = new PageInfo<>(list);
        if (pageInfo != null) {
            return new Msg(200, "").add("pageInfo", pageInfo);
        } else {
            return new Msg(500, "查询失败！");
        }
    }

    //添加
    @RequestMapping("/zhichu/addZhichu.do")
    @ResponseBody
    public Msg addZhichu(Myzhichu entity, HttpServletRequest request) {
        //获取当前的日期
        Date date = new Date();
        //设置要获取到的时间年月日
        SimpleDateFormat time = new SimpleDateFormat("yyyy-MM-dd");
        //获取String类型的时间
        entity.setZtime(time.format(date));
        //获取登录人ID
        Myemp myemp = (Myemp) request.getSession().getAttribute("login");
        entity.setEid(myemp.getEid());
        int i = myzhichuService.addZhichu(entity);
        if (i > 0) {
            return new Msg(200, "成功！");
        } else {
            return new Msg(500, "失败！");
        }
    }

    //根据ID查询
    @RequestMapping("/zhichu/findZhichuByZid.do")
    @ResponseBody
    public Msg findZhichuByZid(Myzhichu entity) {
        Myzhichu list = myzhichuService.findZhichuByZid(entity);
        if (list != null) {
            return new Msg(200, "成功！").add("list", list);
        } else {
            return new Msg(500, "失败！");
        }
    }

    //修改
    @RequestMapping("/zhichu/updateZhichu.do")
    @ResponseBody
    public Msg updateZhichu(Myzhichu entity, HttpServletRequest request) {
        //获取当前的日期
        Date date = new Date();
        //设置要获取到的时间年月日
        SimpleDateFormat time = new SimpleDateFormat("yyyy-MM-dd");
        //获取String类型的时间
        entity.setZtime(time.format(date));
        //获取登录人ID
        Myemp myemp = (Myemp) request.getSession().getAttribute("login");
        entity.setEid(myemp.getEid());
        int i = myzhichuService.updateZhichu(entity);
        if (i > 0) {
            return new Msg(200, "成功！");
        } else {
            return new Msg(500, "失败！");
        }
    }

}

