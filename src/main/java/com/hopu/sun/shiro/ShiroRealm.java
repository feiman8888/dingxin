package com.hopu.sun.shiro;

import com.hopu.sun.mapper.MyempMapper;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.realm.AuthenticatingRealm;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author sun
 * @create 2020-09-05 23:23
 */
public class ShiroRealm extends AuthenticatingRealm {

    @Autowired
    private MyempMapper myempMapper;

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        return null;
    }

    /**
     * 登录的验证实现方法
     * @param
     * @return
     * @throws AuthenticationException
     */
//    @Override
//    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
//        UsernamePasswordToken token1 = (UsernamePasswordToken) token;
//        String username = token1.getUsername();
//        myempMapper.sele
//        if(user == null) {
//            throw new UnknownAccountException("用户名或密码有误！");
//        }
//        if(user.getStatus() == 0) {
//            throw new UnknownAccountException("用户名已被禁用，请联系系统管理员！");
//        }
//
//        /**
//         * principals: 可以使用户名，或d登录用户的对象
//         * hashedCredentials: 从数据库中获取的密码
//         * credentialsSalt：密码加密的盐值
//         * RealmName:  类名（ShiroRealm）
//         */
//        AuthenticationInfo info = new SimpleAuthenticationInfo(user, user.getPazzword(), null, getName());
//        return info; //框架完成验证
//    }
}
