<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>无标题文档</title>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/css/style.css"></link>
		<script type="text/javascript" src="${pageContext.request.contextPath }/js/jquery-1.8.3.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/layer/layer.js"></script>
		<script type="text/javascript">
			var $webName = "${pageContext.request.contextPath}";
		</script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/js/shour/shourupp.js"></script>
		<STYLE type="text/css">
.forminfo1 li {
	margin-bottom: 15px;
	clear: both;
}
</STYLE>
	</head>
	<body>
		<div class="formbody" >
			<ul class="forminfo1">
				<li>
					<label>收入金额：</label>
					<input name="smoney" type="text" id="smoney" class="dfinput1" placeholder="请输入收入金额" />
				</li>
				<li>
					<label>收入条目：</label>
					<select id="stm" name="stm" class="selectinput" style="line-height: 20px;">
					   <option value="客户房租">客户房租</option>
					   <option value="客户物损赔偿">客户物损赔偿</option>
					   <option value="其它">其它</option>
					</select>
				</li>
				
				<li>
					<label>备注说明：</label>
					<textarea name="sremark" id="sremark" rows="5" cols="50"  style="border: solid 1px #E0E0E0;" placeholder="请输入收入来源"></textarea>
				</li>
				<input type="hidden" name="sid" id="sid" value="<%=request.getParameter("sid") %>">
				<li>
					<label>&nbsp;</label>
					<input type="submit" class="btn" value="修改" />
				</li>
			</ul>
		</div>
		<input type="hidden" id="botao">
	</body>
</html>
