<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>无标题文档</title>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/css/style.css"></link>
		<script type="text/javascript" src="${pageContext.request.contextPath }/js/jquery-1.8.3.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/layer/layer.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/js/ajaxfileupload.js"></script>
		<script type="text/javascript">
			var $webName="${pageContext.request.contextPath}";
		</script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/js/djtf/djtfupp.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/js/My97DatePicker/WdatePicker.js"></script>
		<STYLE type="text/css">
.forminfo1 li {
	margin-bottom: 15px;
	clear: both;
}
</STYLE>
	</head>
	<body>
		<div class="formbody" >
			<ul class="forminfo1">
				
				
				<li>
					<label>退租金：</label>
					<input type="text" name="tyzj" id="tyzj" class="selectinput">&nbsp;/元
				</li>
				<li>
					<label>退租日期 ：</label>
					<input type="text" name="mtime" id="time" class="selectinput" onClick="WdatePicker({el:this,dateFmt:'yyyy-MM-dd'})" readonly="readonly">
				</li>
				<li>
				    <label>备注说明 ：</label>
					<textarea name="mremark" id="mremark" rows="5" cols="50" style="border: solid 1px #E0E0E0;" placeholder="请输入备注说明"></textarea>
				</li>
				<input type="hidden" name="mid" id="mid" value="<%=request.getParameter("mid") %>">
				<li>
					<label>&nbsp;</label>
					<input type="submit" class="btn" value="确定" />
				</li>
			</ul>
		</div>
		<input type="hidden" id="botao">
	</body>
</html>
