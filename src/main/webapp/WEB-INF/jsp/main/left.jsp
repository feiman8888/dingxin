<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<link href="${pageContext.request.contextPath }/css/style.css"
			rel="stylesheet" type="text/css" />
		<script language="JavaScript"
			src="${pageContext.request.contextPath }/js/jquery.js">
</script>
		<script type="text/javascript">
$(function() {
	//导航切换
	$(".menuson .header").click(
			function() {
				var $parent = $(this).parent();
				$(".menuson>li.active").not($parent).removeClass("active open")
						.find('.sub-menus').hide();

				$parent.addClass("active");
				if (!!$(this).next('.sub-menus').size()) {
					if ($parent.hasClass("open")) {
						$parent.removeClass("open").find('.sub-menus').hide();
					} else {
						$parent.addClass("open").find('.sub-menus').show();
					}

				}
			});

	// 三级菜单点击
	$('.sub-menus li').click(function(e) {
		$(".sub-menus li.active").removeClass("active")
		$(this).addClass("active");
	});

	$('.title').click(function() {
		var $ul = $(this).next('ul');
		$('dd').find('.menuson').slideUp();
		if ($ul.is(':visible')) {
			$(this).next('.menuson').slideUp();
		} else {
			$(this).next('.menuson').slideDown();
		}
	});
})
</script>


	</head>

	<body style="background: #fff3e1;">
		<div class="lefttop">
			<span></span>管理导航
		</div>
		<dl class="leftmenu">
			<dd>
				<div class="title">
					<span><img
							src="${pageContext.request.contextPath }/images/leftico01.png" />
					</span>基础资料
				</div>
				<ul class="menuson">
					<li>
						<div class="header">
							<cite></cite>
							<a href="${pageContext.request.contextPath }/sun/myemp/redirect.do?page=part/all" target="rightFrame">部门信息</a><i></i>
						</div>
					</li>
					<li>
						<div class="header">
							<cite></cite><a
								href="${pageContext.request.contextPath }/redirect.do?page=myjs/all"
								target="rightFrame">岗位信息</a><i></i>
						</div>
					</li>
					<li>
						<div class="header">
							<cite></cite>
							<a href="${pageContext.request.contextPath }/sun/myemp/redirect.do?page=emp/all"
								target="rightFrame">员工信息</a>
							<i></i>
						</div>
						<%--<ul class="sub-menus">
							<li>
								<a href="javascript:;">员工信息</a>
							</li>
							<li>
								<a href="javascript:;">片区信息</a>
							</li>
							<li>
								<a href="javascript:;">基本内容</a>
							</li>
							<li>
								<a href="javascript:;">自定义</a>
							</li>
						</ul>
					--%>
					</li>


					<li>
						<div class="header">
							<cite></cite><a
								href="${pageContext.request.contextPath }/sun/myarea/redirect.do?page=area/all"
								target="rightFrame">片区信息</a><i></i>
						</div>
					</li>

					<li>
						<div class="header">
							<cite></cite><a
								href="${pageContext.request.contextPath }/sun/mysort/redirect.do?page=sort/all"
								target="rightFrame">房屋类别</a><i></i>
						</div>
					</li>

					<li>
						<div class="header">
							<cite></cite><a
								href="${pageContext.request.contextPath }/redirect.do?page=house/all"
								target="rightFrame">房屋资料</a><i></i>
						</div>
					</li>
				</ul>
			</dd>

			<dd>
				<div class="title">
					<span><img
							src="${pageContext.request.contextPath }/images/leftico02.png" />
					</span>业务管理
				</div>
				<ul class="menuson">
					<li>
						<div class="header">
							<cite></cite><a
								href="${pageContext.request.contextPath }/redirect.do?page=custom/all"
								target="rightFrame">客户登记</a><i></i>
						</div>
					</li>
					<li>
						<div class="header">
							<cite></cite><a
								href="${pageContext.request.contextPath }/redirect.do?page=djrz/all"
								target="rightFrame">登记入住</a><i></i>
						</div>
					</li>
					<li>
						<div class="header">
							<cite></cite><a
								href="${pageContext.request.contextPath }/redirect.do?page=djsf/all"
								target="rightFrame">登记收费</a><i></i>
						</div>
					</li>
					<li>
						<div class="header">
							<cite></cite>
							<a href="${pageContext.request.contextPath }/redirect.do?page=djtf/all"
								target="rightFrame">登记停租</a><i></i>
						</div>
					</li>
					<li>
						<div class="header">
							<cite></cite><a
								href="${pageContext.request.contextPath }/redirect.do?page=biao/all"
								target="rightFrame">操表记录</a><i></i>
						</div>
					</li>
					<li>
						<div class="header">
							<cite></cite><a
								href="${pageContext.request.contextPath }/redirect.do?page=bs/all"
								target="rightFrame">登记报损</a><i></i>
						</div>
					</li>
				</ul>
			</dd>


			<dd>
				<div class="title">
					<span><img
							src="${pageContext.request.contextPath }/images/leftico03.png" />
					</span>业务报表
				</div>
				<ul class="menuson">
					<li>
						<div class="header">
							<cite></cite><a
								href="${pageContext.request.contextPath }/redirect.do?page=shour/all"
								target="rightFrame">现金收入流水账</a><i></i>
						</div>
					</li>
					<li>
						<div class="header">
							<cite></cite><a
								href="${pageContext.request.contextPath }/redirect.do?page=zhichu/all"
								target="rightFrame">现金支出流水账</a><i></i>
						</div>
					</li>
					<li>
						<div class="header">
							<cite></cite><a
								href="${pageContext.request.contextPath }/redirect.do?page=zsr/all1"
								target="rightFrame">出租房分类查询</a><i></i>
						</div>
					</li>
					<li>
						<div class="header">
							<cite></cite><a
								href="${pageContext.request.contextPath }/redirect.do?page=zsr/all2"
								target="rightFrame">出租费用收入查询</a><i></i>
						</div>
					</li>

					<li>
						<div class="header">
							<cite></cite><a
								href="${pageContext.request.contextPath }/redirect.do?page=dq/all"
								target="rightFrame">到期收租查询</a><i></i>
						</div>
					</li>
				</ul>
			</dd>


			<dd>
				<div class="title">
					<span><img
							src="${pageContext.request.contextPath }/images/leftico04.png" />
					</span>系统维护
				</div>
				<ul class="menuson">
					<li>
						<div class="header">
							<cite></cite><a
								href="${pageContext.request.contextPath }/redirect.do?page=xx/ma"
								target="rightFrame">修改密码</a><i></i>
						</div>
					</li>
					<li>
						<div class="header">
							<cite></cite><a
								href="${pageContext.request.contextPath }/redirect.do?page=xx/bf"
								target="rightFrame">数据备份</a><i></i>
						</div>
					</li>
					<li>
						<div class="header">
							<cite></cite><a
								href="${pageContext.request.contextPath }/redirect.do?page=zc/all"
								target="rightFrame">租房政策</a><i></i>
						</div>
					</li>
					<li>
						<div class="header">
							<cite></cite><a
								href="${pageContext.request.contextPath }/redirect.do?page=ht/all"
								target="rightFrame">合同样本</a><i></i>
						</div>
					</li>
				</ul>

			</dd>

		</dl>

	</body>
</html>
