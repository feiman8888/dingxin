<%@ page language="java" pageEncoding="utf-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
       <link href="${pageContext.request.contextPath }/css/style.css" rel="stylesheet" type="text/css" />
       <script type="text/javascript" src="${pageContext.request.contextPath }/js/jquery-1.8.3.js"></script>
	   <script type="text/javascript" src="${pageContext.request.contextPath}/layer/layer.js"></script>
	   <script type="text/javascript" src="${pageContext.request.contextPath }/js/zsr/myma.js"></script>
	
	<SCRIPT type="text/javascript">
		//////////////权限控制！！！//////////////
		//var $userLevel=parseInt("${loginUser.EAdmin}");//获取登陆者的权限等级
		var $webName="${pageContext.request.contextPath}";
		//$(function(){
		//	var tools=$(".tools");
		//	if($userLevel!=-1){
		//		tools.hide();
		//	}
		//});
</SCRIPT>
	</head>

	<body>

		<div class="place">
			<span>位置：</span>
			<ul class="placeul">
				<li>
					首页
				</li>
				<li>
					修改密码
				</li>
			</ul>
		</div>

		<div class="formbody">

			<div class="formtitle">
				<span>修改密码</span>
			</div>

			<ul class="forminfo">
				<li>
					<label>
						原始密码:
					</label>
					<input name="oldpsw" id="oldpsw" type="text" class="dfinput"  />
				</li>
				<li>
					<label>
						新密码:
					</label>
					<input name="newpsw" id="newpsw" type="text" class="dfinput" />
				</li>
				<li>
					<label>
						再输入一次:
					</label>
					<input name="newpsw1" id="newpsw1" type="text" class="dfinput" />
				</li>
				
				
				<li>
					<label>
						&nbsp;
					</label>
					<input name="" type="button" class="btn" value="确认保存" />
				</li>
			</ul>
		</div>
	</body>
</html>
