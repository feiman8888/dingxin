<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/css/style.css"></link>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/css/font-awesome.min.css"></link>
<link rel="stylesheet" href="${pageContext.request.contextPath }/layui/css/layui.css" />
<script type="text/javascript" src="${pageContext.request.contextPath }/layui/layui.js"></script>
<script src="${pageContext.request.contextPath }/js/jquery-1.8.3.js"></script>
<script src="${pageContext.request.contextPath}/layer/layer.js"></script>
<SCRIPT type="text/javascript">
	//////////////权限控制！！！//////////////
	var $webName = "${pageContext.request.contextPath}";
</SCRIPT>
<script src="${pageContext.request.contextPath}/js/part/mydepty.js"></script>


</head>
<body>
	<!-- 位置信息 -->
	<div class="place">
		<span>位置：</span>
		<ul class="placeul">
			<li><a href="${pageContext.request.contextPath }/main/right.jsp">首页</a></li>
			<li><a href="${pageContext.request.contextPath }/part/all.jsp">部门管理</a></li>
			<li><a href="${pageContext.request.contextPath }/part/all.jsp">基本内容</a></li>
		</ul>
	</div>
	<!-- 内容表格信息 -->
	<div class="rightinfo">
		<div class="tools">
			<ul class="toolbar">
				<li class="addPart"><span style="margin-top:0px;margin-button:0px;"><img src="${pageContext.request.contextPath }/images/t01.png" /> </span>添加</li>
			</ul>
			<ul class="toolbar">
				<li class="delPart"><span style="margin-top:0px;margin-button:0px;"><img src="${pageContext.request.contextPath }/images/t01.png" /> </span>已经撤销部门
				</li>
			</ul>

			<ul class="toolbar">
				<li class="backPart" style="display: none;"><span><img src="${pageContext.request.contextPath }/images/t01.png" /> </span>查询部门
				</li>
			</ul>
		</div>
		<table class="tablelist"></table>
		<%-- 分页位置 --%>
		<div class="pagin" align="center">
			<div class="message"></div>
		</div>
	</div>
</body>
</html>
