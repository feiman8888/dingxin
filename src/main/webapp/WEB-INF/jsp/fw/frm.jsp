<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>

		<link rel="stylesheet"
			href="${pageContext.request.contextPath }/css/style.css"
			type="text/css">
		<script type="text/javascript" language="JavaScript"
			src="${pageContext.request.contextPath }/js/hide.js">
</script>
		<script language="javascript"
			src="${pageContext.request.contextPath }/js/check.js">
</script>
		<script language="javascript"
			src="${pageContext.request.contextPath }/js/checkAll.js">
</script>
		<script language="javascript"
			src="${pageContext.request.contextPath }/js/clientSideApp.js">
</script>
		<script type="text/javascript">
function toHandle(id, pid, treeid, name) {
	list.location = "/org/orgQuery.do?act=q3&queryDept=" + id;
}
function ctrlbar() {
	if (showTree.style.display == "") {
		showTree.style.display = "none";
		menuSwitch.src = "${pageContext.request.contextPath }/images/gray_2_03.gif";//隐藏时图片
	} else {
		showTree.style.display = "";
		menuSwitch.src = "${pageContext.request.contextPath }/images/gray_03.gif";//打开时图片
	}
}
</script>
		<script type="text/javascript">
with (window)
	onload = onresize = function() {
		var mainHeight = document.body.offsetHeight - 25;
		document.getElementById("tree").style.height = "" + mainHeight + "px";
		document.getElementById("rs").style.height = "" + mainHeight + "px";
	}</script>

	</head>

	<body bgcolor="#eff3f7" topmargin="0" leftmargin="0">

		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td height="25" align="center" valign="bottom" class="td06"
					width="18%">
					<table width="98%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td height="25" align="center" valign="bottom" class="td06">
								<div class="place">
									<span>位置：</span>
									<ul class="placeul">
										<li>
											<a href="${pageContext.request.contextPath }/part/all.jsp">房屋类别信息</a>
										</li>
									</ul>
								</div>
							</td>
							<td class="place">
								&nbsp;
							</td>
						</tr>
					</table>
				</td>

				<td align="center" valign="bottom" class="td06" width="82%">
					<div class="place">
						<span>位置：</span>
						<ul class="placeul">
							<li>
								<a href="${pageContext.request.contextPath }/main/right.jsp">首页</a>
							</li>
							<li>
								<a href="${pageContext.request.contextPath }/part/all.jsp">房屋类别信息</a>
							</li>
							<li>
								<a href="${pageContext.request.contextPath }/part/all.jsp">房产详细信息</a>
							</li>
						</ul>
					</div>
				</td>

			</tr>
		</table>


		<table width="100%" height="100%" border="0" cellspacing="0"
			cellpadding="0">
			<tr>
				<td valign="top" id="showTree" width="18%">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td>
								<iframe id="tree" name="tree" frameborder="0" framespacing="0"
									scrolling="auto" width="100%" height="100%"
									src="${pageContext.request.contextPath }/fw/alllb.jsp"></iframe>
							</td>
						</tr>
					</table>
				</td>

				<td width="10" class="td06">
					<table width="10" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td>
								<img
									src="${pageContext.request.contextPath }/images/gray_03.gif"
									width="10" height="53" id="menuSwitch" onClick="ctrlbar();"
									style="cursor: hand">
							</td>
						</tr>

					</table>
				</td>

				<td valign="top">
					<table width="100%" height="100%" border=0 cellpadding="0"
						cellspacing="0">
						<tr>
							<td>
								<iframe id="userframe" name="userframe" frameborder="0"
									framespacing="0" scrolling="auto" width="100%" height="100%"
									src="${pageContext.request.contextPath }/fw/orgQuery.jsp"></iframe>
							</td>

						</tr>
					</table>
				</td>
			</tr>
		</table>
	</body>
</html>
