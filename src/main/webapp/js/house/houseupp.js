var sid=0;
var aid=0;

$(function(){
 
  init();
  //数据验证
 // checkItem();
  //提交表单位
   commitItem();
});
function init() {
	var hid=$("#hid").val(); 
	$.ajax({
		url:$webName+'/house/findHouseById.do',
		data:{hid:hid},
		dataType:'json',
		type:'post',
		success:function(mydata)
		{
			$("#sid").val(mydata.map.list.sid);
			$("#aid").val(mydata.map.list.aid);
			$("#haddress").val(mydata.map.list.haddress);
			$("#hfh").val(mydata.map.list.hfh);
			$("#hhx").val(mydata.map.list.hhx);
			$("#hmj").val(mydata.map.list.hmj);
			$("#hcx").val(mydata.map.list.hcx);
			$("#hmoney").val(mydata.map.list.hmoney);
			$("#hwf").val(mydata.map.list.hwf);
			$("#hdx").val(mydata.map.list.hdx);
			$("#hsf").val(mydata.map.list.hsf);
			$("#hmq").val(mydata.map.list.hmq);
			$("#dkd").val(mydata.map.list.dkd);
			$("#skd").val(mydata.map.list.skd);
			$("#mkd").val(mydata.map.list.mkd);
			$("#hjp").val(mydata.map.list.hjp);
			$("#hremark").val(mydata.map.list.hremark);
			sid=mydata.map.list.sid;
			aid=mydata.map.list.aid;
		}
	});
	
	
	//得到下拉框的值 
	$.ajax({
		url:$webName+'/sun/mysort/sort/findAllSort.do',
		dataType:'json',
		type:'post',
		data:'',
		async : true,
		success:function(mydata)
		{
		   $.each(mydata.map.pageInfo.list,function(index,xx){
			   if(sid==xx.sid)
				   {
				    $("#sid").append("<option value="+xx.sid+" selected='selected'>"+xx.sname+"</option>");
				   }
			   else
				   {
				      $("#sid").append("<option value="+xx.sid+">"+xx.sname+"</option>");
				   }
			   
		   });
		}
	});
	
	$.ajax({
		url:$webName+'/sun/myarea/area/findAllAreaPageData.do',
		dataType:'json',
		type:'post',
		data:'',
		async : true,
		success:function(mydata)
		{

		   $.each(mydata.map.pageInfo.list,function(index,xx){
			   if(aid==xx.aid)
				   {
				     $("#aid").append("<option value="+xx.aid+" selected='selected'>"+xx.aname+"</option>");
				   }
			   else
				   {
				     $("#aid").append("<option value="+xx.aid+">"+xx.aname+"</option>");
				   }
			   
		   });
		}
	});
	
	
	
	
};
/*****************************************/


/******************************提交表单********************************/
function commitItem()
{
	$(".btn").bind("click",function(){
    var hid = $("#hid").val();
	var sid = $("#sid").val();
	var aid = $("#aid").val();
	var haddress = $("#haddress").val();
	var hfh = $("#hfh").val();
	var hhx = $("#hhx").val();
	var hmj = $("#hmj").val();
	var hcx = $("#hcx").val();
	var hmoney = $("#hmoney").val();
	var hwf = $("#hwf").val();
	var hdx = $("#hdx").val();
	var hsf = $("#hsf").val();
	var hmq = $("#hmq").val();
	var dkd = $("#dkd").val();
	var skd = $("#skd").val();
	var mkd = $("#mkd").val();
	var hjp = $("#hjp").val();
	var hremark = $("#hremark").val();
	
	var x1=$("#file").val();
	var x2=$("#file2").val();
	var x3=$("#file3").val();
	
	
	if((x1.length==0&&x2.length!=0 && x3.length!=0)||(x1.length==0&&x2.length==0 &&x3.length!=0)||(x1.length==0&&x2.length!=0 && x3.length==0))
		{
		   parent.layer.msg('必须上传三张相片！',{icon:2});
			 
		}
	else if((x2.length==0&&x1.length!=0 && x3.length!=0)||(x2.length==0&&x1.length==0 && x3.length!=0)||(x2.length==0&&x1.length!=0 && x3.length==0))
		{
		   parent.layer.msg('必须上传三张相片！',{icon:2});
		}
	else if((x3.length==0&&x2.length!=0 && x1.length!=0)||(x3.length==0&&x2.length==0 && x1.length!=0)||(x3.length==0&&x2.length!=0 && x1.length==0))
		{
			parent.layer.msg('必须上传三张相片！',{icon:2});
		}

	else
		{
		$.ajaxFileUpload({
	    url:$webName+'/house/uppHouse.do',
	    secureuri:false,//一般设置为false
	    fileElementId:['file','file2','file3'],//上传对象
	    data:{
	    	  "hid":hid,
			  "sid":sid,
			  "aid":aid,
			  "haddress":haddress,
			  "hfh":hfh,
			  "hhx":hhx,
			  "hmj":hmj,
			  "hcx":hcx,
			  "hmoney":hmoney,
			  "hwf":hwf,
			  "hdx":hdx,
			  "hsf":hsf,
			  "hmq":hmq,
			  "dkd":dkd,
			  "skd":skd,
			  "mkd":mkd,
			  "hjp":hjp,
			  "hremark":hremark


		 }, //上传控件以外的控件对应的参数
	    dataType: 'json', 
	    success:function(mydata,status)
			{
	    	     parent.layer.msg('修改成功!',{icon:1});
		         var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
	             parent.layer.close(index);
			}
	    	  ,
             error: function (data, status, e)//服务器响应失败处理函数
              {
	    	      
	    	      parent.layer.msg('修改成功!',{icon:1});
		    	  var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
	              parent.layer.close(index);
                                 
               }
	       });
		}
	});
}