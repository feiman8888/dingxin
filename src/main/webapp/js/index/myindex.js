$(function() {
	$(".loginuser").val("");
	$(".loginpwd").val("");		
	$('.loginbox').css( {
		'position' : 'absolute',
		'left' : ($(window).width() - 692) / 2
	});
	$(window).resize(function() {
		$('.loginbox').css( {
			'position' : 'absolute',
			'left' : ($(window).width() - 692) / 2
		});
	});
	$(".loginuser").focus();	
	getLogin();//失去焦点事件	
	mysub();//提交事件	
	getWj();//忘记密码
});
function getLogin(){
	$(".loginuser").bind("blur",function(){
		 var uname=$(".loginuser").val();		 
		 if(uname.length==0){
			 layer.tips('对不起账号不能为空！','.loginuser',{tips:[2,'red']});
		 }else{
			 $.ajax({
				url:'/sun/myemp/findByEname.do',
				dataType:'json',
				type:'post',
				data:{ename:uname},
				async : true,
				success:function(mydata) {
					console.log(mydata);
					if(mydata.code==200){//有这个人,不做任何操作
					}else{
						layer.tips('您输入账号不存在！','.loginuser',{tips:[2,'red']});
					}
				}
			});
		}
	});
}


function mysub(){
	$(".loginbtn").click(function(){
		var name = $(".loginuser").val();
		var psw = $(".loginpwd").val();
		if(name.length==0)  {
			layer.tips('账号不能为空！','.loginuser',{tips:[2,'red']});
			$(".loginuser").focus();
			return false;
		} else if(psw.length==0) {
			layer.tips('密码不能为空！','.loginpwd',{tips:[2,'red']});
			$(".loginuser").focus();
			return false;
		} else {
			var mypart = "ename=" + name + "&epsw=" + psw+ "";
			var i = layer.load(0);
			$.post('/sun/myemp/login.do',mypart,function(mydata){
				layer.close(i);
				if(mydata!=null){		
					window.location.href="/sun/myemp/redirect.do?page=main/main";
				}else {
					layer.tips('密码错误！','.loginpwd',{tips:[2,'red']});
			    	$(".loginuser").focus();
			     	return false;
				}
				},'json'
			);
		}
	});
}


function getWj()
{
	$(".look").click(function(){
		
		layer.open( {
			type : 2,
			title : "忘记密码",
			fix : false,
			shadeClose : true,
			area : [ '300px', '100px' ],
			content : './xx/bf1.jsp',
			skin : 'layui-layer-lan',
			shift: 4, 
			success : function(layero, index) {
				layer.style(index, {
					width : '800px',
					height : '500px',
					top : '100px',
					left : ($(window).width() - 500) / 2
				});
			},
			end : function() {
				//getAll(current);
			}
		});
	});
}