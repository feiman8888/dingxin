var current = 1;
var up = 0;
var next = 0;
var allpages = 0;
var allcount = 0;
var editFlag = 0; //部门操作的表示1为添加部门 2为修改部门
var pflag = 0;
$(function() {
	//加载页面数据
	initJs();
	//点击首页
	getFirst();
	//点击上一页
	getUp();
	//点击下一页
	getNext();
	//点击尾页
	getLast();
	//点击转到第几页
	getZhan();
	//添加角色
	addJs();
	//弹出修改页面
	outupdate();
	//删除
	//mydel();
	//查询已经撤销的部门
	//mydelAll();
	//返回查询
	//getBack();
	//恢复部门
	//getHuiFu();
	getSeAll();

});

/***********************************/
function initJs() {
	$(".tablelist,.tablelist th").css("text-align", "center");
	getAll(1);
}

/***********************************************/
function getAll(current) {
	$(".tablelist").empty();
	$(".tablelist").append("<thead><tr align='center'><th  align='center'>编号</th><th  align='center'>支出金额</th><th  align='center'>支出条目</th><th  align='center'>支出说明</th><th  align='center'>支出时间</th><th  align='center'>经办人</th><th  align='center'>修改</th></tr></thead>");

	$.ajax({
		url : $webName + '/zhichu/findAllzhichu.do',
		dataType : 'json',
		data : {
			current : current
		},
		type : 'post',
		success : function(mydata) {
			if (mydata.code == "500") {
				window.location.href = "../error.jsp";
			} else {

				//分页需要的参数
				up=mydata.up;
				next=mydata.next;
				allpages=mydata.map.pageInfo.pages;
				current=mydata.map.pageInfo.pageNum;
				allcount=mydata.map.pageInfo.total
				$.each(mydata.map.pageInfo.list, function(index, xx) {
					if (xx.zid != null) {
						var srt = '<tr><td>' + xx.zid + '</td>';
						srt += '<td>' + xx.zmoney + '</td>';
						srt += '<td>' + xx.ztm + '</td>';
						srt += '<td style="text-align:left;">' + xx.zremark + '</td>';
						srt += '<td>' + xx.ztime + '</td>';
						srt += '<td>' + xx.myemp.erealname + '</td>';
						srt += '<td><a href="#" class="myupp">修改</a></td>';
						$(".tablelist").append(srt);
					}
				});
				//加尾巴
				if (mydata.list.length > 0) {
					$(".pagin > .message").empty().append('统计：共<i class="blue"> ' + allcount + '</i> 条记录， 共<i class="blue"> ' + allpages + ' </i>页，当前显示第&nbsp;<i class="blue">' + current + '/' + allpages + '&nbsp;</i>页');
					$(".pagin > .message").append('<ul class="paginList">');
					$(".paginList").append('<li class="paginItem"><a href="javascript:void(0)" class="first">首页</a></li>');
					$(".paginList").append('<li class="paginItem"><a href="javascript:void(0)" class="up">上一页</a></li>');
					$(".paginList").append('<li class="paginItem"><a href="javascript:void(0)" class="next">下一页</a></li>');
					$(".paginList").append('<li class="paginItem"><a href="javascript:void(0)" class="last">尾页</a></li>');
					$(".pagin > .message").append('<SPAN style="float: right;margin-top:8px;"> 转到第 <select class=select></select> 页</SPAN>');
					//给下拉框赋值
					for (var i = 1; i <= allpages; i++) {
						$(".select").append("<option value='" + i + "'>" + i + "</option>");
					}
					//你当前正处在哪一页，就应该让几选中
					//你当前正处在哪一页，就应该让几选中
					$(".select option:eq(" + (parseInt(current) - 1) + ")").attr('selected', true);
				} else {
					var srt = '<tr><td colspan="7">暂时没有支出记录!</td></tr>';
					$(".tablelist").append(srt);
				}
			}
		}
	});
}
/**************************************************/
function getFirst() {
	$(".first").live("click", function() {
		getAll(1);
	});
}

function getUp() {
	$(".up").live("click", function() {
		getAll(up);
	});
}
function getNext() {
	$(".next").live("click", function() {
		getAll(next);
	});
}
function getLast() {
	$(".last").live("click", function() {
		getAll(allpages);
	});
}
function getZhan() {
	$(".select").live("change", function() {
		getAll($(".select").val());
	});
}
/********************************************/
//添加角色 
function addJs() {
	//点击添加按钮的绑定cilck事件	
	$(".addPart").bind("click", function() {
		layer.open({
			type : 2,
			title : "添加支出",
			fix : false,
			shadeClose : true,
			sr : [ '300px', '100px' ],
			content : $webName + '/redirect.do?page=zhichu/add',
			skin : 'layui-layer-lan',
			shift : 4, //动画类型
			//弹出来后，设定层出现的位置
			success : function(layero, index) {
				layer.style(index, {
					width : '500px',
					height : '500px',
					top : '100px',
					left : ($(window).width() - 500) / 2
				});
			},
			//层被销毁回调
			end : function() {
				getAll(current);
			}
		});
	});
}
;
/**************************************************/


/************************弹出修改页面****************************/
function outupdate() {
	$(".myupp").live("click", function() {

		var zid = $(this).parents("tr").children("td").eq(0).text();
		layer.open({
			type : 2,
			title : '修改支出',
			fix : false,
			shadeClose : true,
			sr : [ '300px', '100px' ],
			content : $webName + '/redirect.do?page=zhichu/upp&zid=' + zid,
			skin : 'layui-layer-lan',
			shift : 4, //动画类型
			//弹出来后，设定层出现的位置
			success : function(layero, index) {
				layer.style(index, {
					width : '500px',
					height : '500px',
					top : '100px',
					left : ($(window).width() - 500) / 2
				});
			},
			//层被销毁回调
			end : function() {
				getAll(current);
			}
		});
	});
}
/*********************************************/

function getSeAll() {
	$(".seAll").click(function() {
		/*window.location.href = "zhicAction_getSe.action";*/
		$.ajax({
			url : $webName + '/zhichu/getBar3D.do',
			dataType : 'json',
			type : 'post',
			data : '',
			success : function(mydata) {
				$(".place").remove();
				$(".rightinfo").empty();
				var srt = '<img src="'+$webName+'/charts/tubiao.jpeg">';
				$("#tubiao").append(srt);
			}
		});
	});
}