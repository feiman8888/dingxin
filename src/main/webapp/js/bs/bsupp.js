var hid = 0;
var aid = 0;
$(function() {

	init();
	//数据验证
	myaddSe();
	//提交表单位
	commitItem();
});
function init() {
	$("#dkd").focus();

	var bid = $("#bid").val();
	//查询基本信息
	$.ajax( {
		url : $webName + '/bs/findBsByBid.do',
		data : {
			bid : bid
		},
		dataType : 'json',
		type : 'post',
		success : function(mydata) {
			$("#bremark").val(mydata.map.list.bremark);
			hid = mydata.map.list.myhouse.hid;
			aid = mydata.map.list.myhouse.aid;

			/**************得到下拉框的值 *************/
			$.ajax( {
				url : $webName + '/sun/myarea/area/findAllAreaPageData.do',
				dataType : 'json',
				type : 'post',
				data : '',
				async : true,
				success : function(mydata) {
					$.each(mydata.map.pageInfo.list, function(index, xx) {
						if (aid == xx.aid) {
							$("#aid").append(
									"<option value=" + xx.aid
											+ " selected='selected'>"
											+ xx.aname + "</option>");
						} else {
							$("#aid").append(
									"<option value=" + xx.aid + ">" + xx.aname
											+ "</option>");
						}
					});
				}
			});
			/*****************************/

			/**************得到房子信息***********/
			$.ajax( {
				url : $webName + '/house/findAllHousePD.do',
				dataType : 'json',
				type : 'post',
				data : {
					aid : aid
				},
				async : true,
				success : function(mydata) {
					$.each(mydata.map.pageInfo.list, function(index, xx) {
						if (hid == xx.hid) {
							$("#hid").append(
									"<option value=" + xx.hid
											+ " selected='selected'>"
											+ xx.haddress + "</option>");
						} else {
							$("#hid").append(
									"<option value=" + xx.hid + ">"
											+ xx.haddress + "</option>");
						}

					});
				}
			});
			/*****************************/
		}
	});

};
/*****************************************/

function myaddSe() {
	$("#aid").bind(
			"change",
			function() {
				var aid = $(this).val();
				$("#hid").empty();
				if (aid != 0) {

					$.ajax( {
						url : $webName + '/house/findAllHouse.do',
						dataType : 'json',
						type : 'post',
						data : {
							aid : aid
						},
						async : true,
						success : function(mydata) {
							$.each(mydata, function(index, xx) {
								$("#hid").append(
										"<option value=" + xx.hid + ">"
												+ xx.haddress + "</option>");
							});
						}
					});
				} else {
					$("#hid").append("<option value=0>---请选择房屋---</option>");
				}

			});
}


/******************************提交表单********************************/
function commitItem() {
   $(".btn").bind("click",function(){
	var hid = $("#hid").val();
	var bremark = $("#bremark").val();
	var bid=$("#bid").val();

	if(hid.length==0)
		{
		   layer.tips('房子名称不能为空！','#hid',{tips:[2,'red']});
		   $("#hid").focus();
		   return false;
		}
	else if(hid==0)
		{
		   layer.tips('房子名称不能为空！','#hid',{tips:[2,'red']});
		   $("#hid").focus();
		   return false;
		}
	else if(bremark.length==0)
		{
		  layer.tips('备注不能为空！','#bremark',{tips:[2,'red']});
		  $("#bremark").focus();
		  return false;
		}
	
	else
		{
		   var mypart = "hid="+hid+"&bremark="+bremark+"&bid="+bid;
		   var i = layer.load(0);
		   $.post( $webName + '/bs/updateBs.do',mypart,function(mydata){
			 layer.close(i);
			 if(mydata.code==200)
				 {
				   parent.layer.msg('修改成功！', {icon : 6,time : 3000});
				   var index = parent.layer.getFrameIndex(window.name); //获取窗口索引(真正的关 )
				   parent.layer.close(index);
				 }
			 else
				 {
				     parent.layer.msg('修改失败', 2, 9);
				 }
		   },'json');
		}
	});
}