$(function() {
	commitItem();
});

/******************************提交表单********************************/
function commitItem() {
	$(".btn").bind("click", function() {

		var cname = $("#cname").val();
		var csex = $('input:radio[name="csex"]:checked').val();

		var ctel = $("#ctel").val();
		var ctel1 = $("#ctel1").val();
		var ccard = $("#ccard").val();

		if (cname.length == 0) {
			layer.tips('客户姓名不能为空！', '#cname', {
				tips : [ 2, 'red' ]
			});
			$("#cname").focus();
			return false;
		} else if (ctel.length == 0) {
			layer.tips('客户电话不能为空！', '#ctel', {
				tips : [ 2, 'red' ]
			});
			$("#ctel").focus();
			return false;
		} else if (ccard.length == 0) {
			layer.tips('客户身份证不能为空！', '#ccard', {
				tips : [ 2, 'red' ]
			});
			$("#ccard").focus();
			return false;
		} else {
			var mypart = "cname=" + cname + "&csex=" + csex + "&ctel=" + ctel + "&ctel1=" + ctel1 + "&ccard=" + ccard + "";
			var i = layer.load(0);
			$.post($webName + '/custom/addCus.do', mypart, function(mydata) {
				layer.close(i);
				if (mydata.code == 200) {
					parent.layer.msg('增加成功！', {
						icon : 6,
						time : 3000
					});
					var index = parent.layer.getFrameIndex(window.name); //获取窗口索引(真正的关 )
					parent.layer.close(index);
				} else {
					parent.layer.msg('增加失败', 2, 9);
				}
			}, 'json');
		}
	});
}