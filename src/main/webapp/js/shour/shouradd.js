$(function() {
	init();
	commitItem();
});
/****************获得焦点********************/
function init() {
	$("#smoney").focus();
}
;

function commitItem() {
	$(".btn").bind("click", function() {
		var smoney = $("#smoney").val();
		var stm = $("#stm").val();
		var sremark = $("#sremark").val();
		var myage = /^[0-9]*$/; //正则表达式
		var m = myage.test(smoney);


		if (smoney.length == 0) {
			layer.tips('收入金额不能为空！', '#smoney', {
				tips : [ 2, 'red' ]
			});
			$("#smoney").focus();
			return false;
		} else if (!m) {
			layer.tips('收入金额只能为正数！', '#smoney', {
				tips : [ 2, 'red' ]
			});
			$("#smoney").focus();
			return false;
		} else if (sremark.length == 0) {
			layer.tips('收入备注不能为空！', '#sremark', {
				tips : [ 2, 'red' ]
			});
			$("#sremark").focus();
			return false;
		} else {
			var mypart = "smoney=" + smoney + "&stm=" + stm + "&sremark=" + sremark;
			var i = layer.load(0);
			$.post($webName + '/shouru/addShouru.do', mypart, function(mydata) {
				layer.close(i);
				if (mydata.code == 200) {
					parent.layer.msg('添加成功！', {
						icon : 6,
						time : 3000
					});
					var index = parent.layer.getFrameIndex(window.name); //获取窗口索引(真正的关 )
					parent.layer.close(index);
				} else {
					parent.layer.msg('增加失败', 2, 9);
				}
			}, 'json');
		}
	});
}