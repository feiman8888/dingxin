$(function(){
 
  init();
  //数据验证
  checkItem();
  //提交表单位
   commitItem();
});
function init() {
	var sid=$("#sid").val(); 
	$.ajax({
		url:$webName+'/sun/mysort/sort/findSortByNameOrId.do',
		data:{sname:null,sid:sid},
		dataType:'json',
		type:'post',
		success:function(mydata)
		{
			$("#sname").val(mydata.map.list.sname);
			
			$("#sid").val(mydata.map.list.sid);
		}
	});
	
};
/*****************************************/
/******************失去焦点事件****************************/
function checkItem()
{
	$("#sname").focusout(function(){
		var sname=$("#sname").val();
		if(sname.length==0)
			{
			   layer.tips('类型名称不能为空！','#sname',{tips:[2,'red']});
			}
		else
			{
			   $.ajax({
				   url:$webName+'/sun/mysort/sort/findSortByNameOrId.do',
				   dataType:'json',
				   type:'post',
				   data:{sname:sname,sid:null},
				   async : true,
				   success:function(mydata)
				   {
					   if(mydata.code==500)
						   {
						      $("#sname").addClass("newsuccess");
					          $("#sname").removeClass("newerror");
						   }
					   else
						   {
						       layer.tips('对不起类型已存在！','#sname',{tips:[2,'red']});
						   }
					   $("#botao").val(mydata);
				   }
			   });
			}
	});
}



/******************************提交表单********************************/
function commitItem()
{
	$(".btn").bind("click",function(){	
	var sname = $("#sname").val();
	var sid=$("#sid").val();
	if(sname.length==0)
		{
		   layer.tips('类型名称不能为空！','#sname',{tips:[2,'red']});
		   $("#sname").focus();
		   return false;
		}
	else if($("#botao").val()>0)
		{
		  layer.tips('对不起类型已存在！','#sname',{tips:[2,'red']});
		  $("#sname").focus();
		  return false;
		}
	else
		{
		   var mysort = "sname=" + sname + "&sid="+sid;
		   var i = layer.load(0);
		   $.post($webName+'/sun/mysort/sort/uppSort.do',mysort,function(mydata){
			 layer.close(i);
			 if(mydata.code==200)
				 {
				   parent.layer.msg('修改成功！', {icon : 6,time : 3000});
				   var index = parent.layer.getFrameIndex(window.name); //获取窗口索引(真正的关 )
				   parent.layer.close(index);
				 }
			 else
				 {
				     parent.layer.msg('修改失败', 2, 9);
				 }
		   },'json');
		}
	});
}